#!/bin/sh
# usage: $0 version outfile
set -eux

GOLANG_VERSION=1.18

PROGBASE=$(d=$(dirname -- "${0}"); cd "${d}" && pwd)

V="${1}"
O="${2}"

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y install lsb-release

>deps
if [ Xbullseye = X$(lsb_release -cs) ]; then
	export PATH="/usr/lib/go-${GOLANG_VERSION}/bin:${PATH}"
	echo deb http://deb.debian.org/debian bullseye-backports main >/etc/apt/sources.list.d/backports.list
	sed -e "s|^golang$|golang-${GOLANG_VERSION}|" deps.txt >deps
	apt-get update
fi

apt-get -y upgrade
sort -u "deps.txt" deps | xargs apt-get -y install

wget "https://github.com/lxc/lxd/releases/download/lxd-${V}/lxd-${V}.tar.gz"
gzip -cd "lxd-${V}.tar.gz" | tar -f- -x --no-same-owner

cd "lxd-${V}"
make deps

vendor="${PROGBASE}/lxd-${V}/vendor"
export CGO_CFLAGS="-I${vendor}/raft/include/ -I${vendor}/dqlite/include/"
export CGO_LDFLAGS="-L${vendor}/raft/.libs -L${vendor}/dqlite/.libs/"
export LD_LIBRARY_PATH="${vendor}/raft/.libs/:${vendor}/dqlite/.libs/"
export CGO_LDFLAGS_ALLOW="(-Wl,-wrap,pthread_create)|(-Wl,-z,now)"

make


# Packaging

mkdir -p "${PROGBASE}/rootfs/opt/lxd-${V}/bin"
mkdir    "${PROGBASE}/rootfs/opt/lxd-${V}/lib"

cp ~/go/bin/* "${PROGBASE}/rootfs/opt/lxd-${V}/bin"
cp "${vendor}/dqlite/.libs/libdqlite.so"* "${PROGBASE}/rootfs/opt/lxd-${V}/lib"
cp "${vendor}/raft/.libs/libraft.so"* "${PROGBASE}/rootfs/opt/lxd-${V}/lib"

cat >"${PROGBASE}/rootfs/opt/lxd-${V}/bin/lxc.wrapper"<<__EOF__
#!/bin/sh
set -eu
export LD_LIBRARY_PATH=/opt/lxd-${V}/lib
exec /opt/lxd-${V}/bin/lxc "\${@}"
__EOF__

cat >"${PROGBASE}/rootfs/opt/lxd-${V}/bin/lxd.wrapper"<<__EOF__
#!/bin/sh
set -eu
export PATH="/opt/lxd-${V}/bin:\${PATH}"
export LD_LIBRARY_PATH=/opt/lxd-${V}/lib
exec /opt/lxd-${V}/bin/lxd "\${@}"
__EOF__

chmod 0555 "${PROGBASE}/rootfs/opt/lxd-${V}/bin"/*
chmod 0444 "${PROGBASE}/rootfs/opt/lxd-${V}/lib"/*

cp -r "${PROGBASE}/etc" "${PROGBASE}/rootfs/opt/lxd-${V}"

tar -C "${PROGBASE}/rootfs" --owner=root --group=root -f- -c . | gzip -9c >"${O}"
